V.0.0.3-SNAPSHOT
* API Gateway Implementation
* Added tests for get_excitations
* double excitations implementation
* end to end integration test added - 100000 64 bit cu determinants

V.0.0.2
* Added logo to readme.md
* Fixed detreader bug looking for an invalid file path
* Increased test coverage - n_excitations & get_single_excitations output verified for 64bit strings

V.0.0.1
* Added Licence
* Updated Readme