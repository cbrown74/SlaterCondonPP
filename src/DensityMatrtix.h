//
// Created by chris on 12/03/2018.
//
#ifndef SLATERCONDONPP_DENSITY_MATRIX_H
#define SLATERCONDONPP_DENSITY_MATRIX_H

#include <bitset>
#include <vector>
#include <iostream>
#include "CountLeadingZeros.h"
#include "CountTrailingZeros.h"
#include "NExcitations.h"
#include "get_excitation.h.h"
#include "ThreeDimensionArrayAllocator.h"
#include "BitStringDecrementor.h"
#include "DetReader.h"

template <int moNum> class DensityMatrix {


public:
    vector<vector<double> >computeDensityMatrix(int start, int end, std::vector<long double> coef, std::pair<std::vector<std::bitset<moNum>>, std::vector<std::bitset<moNum>>> det) {



        int nDet;

        BitStringDecrementor<moNum> bitStringDecrementor;
        std::bitset<moNum>one(1);




        ArrayAllocator a;
        int ***array = a.allocate(3,2,2);
        long double phase, ccc;

        CountLeadingZeros<int, moNum> c;
        CountTrailingZeros<int, moNum> cc;
        vector<vector<double> > densityMatrix;

        densityMatrix.resize(moNum);
        for (int i = 0; i < moNum; ++i)
            densityMatrix[i].resize(moNum);
        for (int x = 0; x < moNum; ++x) {
            for (int y = 0; y < moNum; ++y) {
                densityMatrix[x][y] = 0;
            }
        }

        unsigned long long iShift = 0;
        unsigned long long j,i,k,l;
        NExcitations<moNum> nexc;
        ExcitationCalculator<moNum>getexc;
        std::bitset<moNum> bitbuffer;
        std::bitset<moNum> zero;
        unsigned long long int  degree = 0;

        for (int detPairIndex = start; detPairIndex < end; detPairIndex++) {

            unsigned long long int iShift = 0;
            bitbuffer = det.first.at(detPairIndex);

            while (bitbuffer != zero) {
                j = cc.trailing_zero_naive(bitbuffer) + iShift + 1;
                densityMatrix[j][j] +=
                         coef.at(detPairIndex) * coef.at(detPairIndex);
                bitbuffer &= bitStringDecrementor.decrement(bitbuffer);

            }
            bitbuffer = det.second.at(detPairIndex);
            while (bitbuffer != zero) {
                j = cc.trailing_zero_naive(bitbuffer) + iShift + 1;
                densityMatrix[j][j] +=  coef.at(detPairIndex) * coef.at(detPairIndex);

                bitbuffer &= bitStringDecrementor.decrement(bitbuffer);

            }



            for (int l = 0; l < detPairIndex ; l++) {
                if (nexc.getExcitations(det.first.at(detPairIndex), det.second.at(detPairIndex), det.first.at(l),
                                        det.second.at(l)) != one) {
                    continue;
                }

                getexc.getExcitation(det.first.at(detPairIndex), det.second.at(detPairIndex), det.first.at(l),
                                     det.second.at(l), array, degree, phase);

                if (array[0][0][0] == 1) {
                    i = array[1][0][0];
                    j = array[1][1][0];
                } else {
                    i = array[1][0][1];
                    j = array[1][1][1];
                }
                ccc = phase * coef.at(detPairIndex) * coef.at(l);


                densityMatrix[j][i] = densityMatrix[j][i] + ccc;
                densityMatrix[i][j] = densityMatrix[i][j] + ccc;
            }


        }

        delete (array);
        return densityMatrix;
    }
};





#endif //SLATERCONDONPP_DENSITY_MATRIX_H
