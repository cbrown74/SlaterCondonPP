//
// Created by chris on 05/12/2017.
//

#ifndef UNTITLED1_COUNTLEADINGZEROS_H
#define UNTITLED1_COUNTLEADINGZEROS_H
//#include <EASTL/bitset.h>

#include <bitset>

template <typename ReturnType, int moNum> class CountLeadingZeros {
//
//public: size_t trailing_zero_eastl(eastl::bitset<moNum> bs)
//    {
////        std::bitset<moNum> n(1);
////        ReturnType t = 0;
////        while ((x & n) == 0)
////        {
////            x = x >> 1;
////            t++;
////        }
////        return t;
//       // x.flip();
//
//
//
//        //bs>>=1;
//        return  moNum - (bs.DoFindFirst()+1);
//
////        for (int i = 0; i < moNum; i ++) {
////            if( x[i]==1){
////                break;
////            }
////            t++;
////        }
//      //  return t;
//    }


public: ReturnType leading_zero_naive(std::bitset< moNum > x)
    {
        ReturnType t = 0;
        for (int i = 0; i < moNum; i ++) {
            if( x[(moNum-1)-i]==1){
                break;
            }
            t++;
        }
        return t;
    }

};


#endif //UNTITLED1_COUNTLEADINGZEROS_H
