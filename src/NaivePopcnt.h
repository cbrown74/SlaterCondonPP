//
// Created by chris on 30/12/2017.
//

#ifndef UNTITLED1_NAIVEPOPCNT_H
#define UNTITLED1_NAIVEPOPCNT_H

#include <iostream>

template<int moNum> class NaivePopcnt{

 //   http://www.cplusplus.com/forum/beginner/119203/
//    int count = 0 , i;
//    for( i = 0; n; ++i )
//    {
//        count += n & 1;
//        n >>= 1;
//    }
//
//    cout << "Bits set: " << count << endl;
//    return( i);


#define CHECK_BIT(var,pos) (((var)>>(pos)) & 1)

public: unsigned long long naivePopcnt(std::bitset<moNum> bitset)
{

        unsigned long long count = 0;


            for(int i = 0; i < 64; i++){


            if(bitset[i]==1){
               count ++;
            }

        }
        return count;

}

public: unsigned long long bruteForceAdd(std::bitset<64> bitset)
    {
//

        //Brute Force Add all 64-bits
        unsigned long long count = 0;

        for(int i = 0; i < 64; i++){

            if(bitset[i]==1){
                count ++;
            }

        }
        return count;

    }




public: unsigned long long brianKernighanPopcnt(std::bitset<64> bitset) {

        // Consecutibely reset LS1b in a loop and count loop cycles
        // until bitset empty
        // The C Programming_Language, 2nd Edition 1988, exercise 2-9
        unsigned long long n = bitset.to_ullong();
        unsigned long long i;
        for (i = 0; n; ++i) {
            n &= n - 1;
        }

        return (i);

    }
};

#endif //UNTITLED1_NAIVEPOPCNT_H
