//
// Created by Brown, Chris on 01/04/2018.
//

#ifndef SLATERCONDONPP_BITSTRINGDECREMENTOR_H
#define SLATERCONDONPP_BITSTRINGDECREMENTOR_H
#include <bitset>

template <int moNum> class BitStringDecrementor{

public :std::bitset<moNum>  decrement(std::bitset<moNum> a) {

        int pos;
        for(int i = 0 ; i < 64; i ++){

            if(a[i]==1){
                pos = i-1;
                a[i]=0;
                break;
            }
        }

        for(int i = pos ; i >= 0; i --){
            if(a[i]==0){
                a[i]=1;
            }else{
                a[i]=0;
                break;
            }
        }
        return a;


    }
};

#endif //SLATERCONDONPP_BITSTRINGDECREMENTOR_H
