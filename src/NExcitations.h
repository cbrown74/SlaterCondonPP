//
// Created by chris on 30/12/2017.
//

#ifndef UNTITLED1_N_EXCITATIONS_H
#define UNTITLED1_N_EXCITATIONS_H

#include <bitset>
#include <iostream>
//Dynamically sized n_excitations function
template <int moNum> class NExcitations{


//public: std::bitset<moNum> getExcitations
//            (std::bitset<moNum> det1up, std::bitset<moNum> det1down,
//             std::bitset<moNum>det2up, std::bitset<moNum>det2down){
//
//        // ^=  is equal to XoR
//        // .count() is equal to popcount
//        std::bitset<moNum> r (std::bitset<moNum>( det1up ^ det2up).count()
//                              + std::bitset<moNum>( det1down ^ det2down).count());
//        //divide result by two
//        r = std::bitset<moNum>(r >> 1);
//        return r;
//        }
//    };


public: std::bitset<moNum> getExcitations
            (std::bitset<moNum> det1up, std::bitset<moNum> det1down,
             std::bitset<moNum> det2up, std::bitset<moNum> det2down){

        // ^=  is equal to XoR
        // .count() is equal to popcount
        std::bitset<moNum> r ((( det1up ^ det2up).count()
                     + ( det1down ^ det2down).count())>>1);
        //divide result by two
       // r = std::bitset<moNum>(r >> 1);
        return r;

    }
};
#endif //UNTITLED1_N_EXCITATIONS_H
