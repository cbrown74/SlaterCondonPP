//
// Created by chris on 19/03/2018.
// Return difference between floating point numbers
//

#ifndef SLATERCONDONPP_RELDIF_H
#define SLATERCONDONPP_RELDIF_H

#define Abs(x)    ((x) < 0 ? -(x) : (x))
#define Max(a, b) ((a) > (b) ? (a) : (b))
class RelDif{
public: double dif(double a, double b)
{
    double c = Abs(a);
    double d = Abs(b);

    d = Max(c, d);

    return d == 0.0 ? 0.0 : Abs(a - b) / d;
}
};

#endif //SLATERCONDONPP_RELDIF_H
