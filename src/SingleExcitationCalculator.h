//
// Created by chris on 01/01/2018.
//

#ifndef UNTITLED1_GET_SINGLE_EXCITATION_H
#define UNTITLED1_GET_SINGLE_EXCITATION_H

#include <array>
#include <bitset>
#include "CountLeadingZeros.h"
#include "HoleParticleCalculator.h"



using namespace std;

template <int moNum> class SingleExcitationCalculator {
    HoleParticleCalculator<moNum> holes_and_particles;
public:
    void getSingleExcitation(bitset<moNum> det1up, bitset<moNum> det1down,
                             bitset<moNum> det2up, bitset<moNum> det2down, int ***exc,long double &phase) {

        exc[0][0][0] = 0;
        exc[0][1][0] = 0;
        exc[0][0][1] = 0;
        exc[0][1][1] = 0;

        std::bitset<moNum> det1[] = {det1up, det1down};
        std::bitset<moNum> det2[] = {det2up, det2down};
        std::bitset<moNum> tmp, particle, hole, j;


        BitStringDecrementor<moNum> bitStringDecrementor;
        long int nperm =0;
        int tz;
        CountTrailingZeros<int, moNum> c;




        long double phase_dble[] = {1, -1};
        for (int i = 0; i < 2; i++) {

            if (det1[i] == det2[i]) {
                continue;
            }

            tmp = det1[i] ^ det2[i];
            particle = tmp & det2[i];
            hole = tmp & det1[i];


            std::bitset<moNum> zero(0);
            if (particle != zero) {
                tz = c.trailing_zero_naive(particle);
                exc[0][1][i] = 1;
                exc[1][1][i] = tz + 1;
            }
            if (hole != zero) {
                tz = c.trailing_zero_naive(hole);
                exc[0][0][i] = 1;
                exc[1][0][i] = tz + 1;

            }

            if ((exc[0][0][i] & exc[0][1][i] ) == 1) {

              // j, k, nperm;
                std::bitset<moNum> low (std::min(exc[1][0][i], exc[1][1][i]));
                std::bitset<moNum> high (std::max(exc[1][0][i], exc[1][1][i]));
                std::bitset<moNum> nn ((low.to_ullong() - 1) & (moNum-1));
                std::bitset<moNum> m  ((high.to_ullong() - 1) & (moNum-1));

                nperm = (det1[i] &
                        std::bitset<moNum>  (
                                (~((std::bitset<64>(1)<<nn.to_ullong()+1).to_ullong())+1) &
                                                   (std::bitset<64>(1)<<m.to_ullong()).to_ullong()-1
                                              )
                        ).count();
                phase = phase_dble[nperm & 1];
                return;

            }
        }
    }
};


#endif //UNTITLED1_GET_SINGLE_EXCITATION_H
