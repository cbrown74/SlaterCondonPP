//
// Created by chris on 05/12/2017.



#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file


#include "catch.hpp"
#include "CountLeadingZeros.h"
#include "DetReader.h"
#include "NaivePopcnt.h"
#include "NExcitations.h"
#include "get_excitation.h.h"
#include "HoleParticleCalculator.h"
#include "CountTrailingZeros.h"
#include "SingleExcitationCalculator.h"
#include "DensityMatrtix.h"
#include "RelDif.h"
#include "BitStringDecrementor.h"


TEST_CASE( " particles.processHolesAndParticles  - Verify contents of exc with two arbitrary determinants",  "[get_holes_and_particles]"){
    HoleParticleCalculator<64> g;
    bitset<64> det1 (10000);
    bitset<64> det2 (100000000);

    pair<bitset<64>, bitset<64>> p;
    p.first = det1;
    p.second = det2;

    ArrayAllocator a;
    int *** array = a.allocate(3,2,2);

    g.processHolesAndParticles(p, array);

    REQUIRE(array[0][1][0] ==1);
    REQUIRE(array[1][1][0] == 4);
    REQUIRE(array[0][0][0] == 1);
    REQUIRE(array[1][0][0] == 8);
    delete array;
}


TEST_CASE( " Verify get_holes_and_particles.getHolesANdParticles is calculated correctly - four 64bit copper determinants",    "[get_holes_and_particles]" ){
    HoleParticleCalculator<64> g;

    std::bitset<64> det1up (8589949951);
    std::bitset<64> det1down (17179899903);
    std::bitset<64> det2up (4294999551);
    std::bitset<64> det2down(16383);

    REQUIRE(g.getHolesAndParticles(det1up, det2up).first.to_ullong() == 4294984704);
    REQUIRE(g.getHolesAndParticles(det1up, det2up).second.to_ullong() ==   8589935104);
    REQUIRE(g.getHolesAndParticles(det1down, det2down).first.to_ullong() == 2048);
    REQUIRE(g.getHolesAndParticles(det1down, det2down).second.to_ullong() ==   17179885568);
}






TEST_CASE( " get_single_excitation - Verify contents of exc with two 64 bit determinants",      "[get_single_excitation]") {

    SingleExcitationCalculator<64> g;
    const int bitSetSize = 64;

    ArrayAllocator a;
    int *** array = a.allocate(3,2,2);

    bitset<64> det1up(32767);
    bitset<64> det1down(274877923263);
    bitset<64> det2up(32767);
    bitset<64> det2down(16383);

    long double phase = 0;

    g.getSingleExcitation(det1up, det1down, det2up, det2down, array, phase);

    REQUIRE( array[0][0][0] == 0);
    REQUIRE(array[0][0][1] == 1);
    REQUIRE(array[0][1][0] == 0);
    REQUIRE(array[0][1][1] == 1);
    REQUIRE( array[1][0][0] == 0);
    REQUIRE( array[1][0][1] == 39);
    REQUIRE( array[1][1][0] == 0);
    REQUIRE( array[1][1][1] == 7);

    delete array;
}

TEST_CASE( " Trailing zeros is counted correctly from 0-64 trailing zeros ", "[trailing_zero_naive]" ) {
    const int bitSetSize = 64;
    CountTrailingZeros<int, bitSetSize> c;
    bitset<bitSetSize> foo(1);
    for(int i = 0; i < bitSetSize; i++){
        REQUIRE( c.trailing_zero_naive(foo) == (i) );
        foo = foo << 1;
    }
    REQUIRE( c.trailing_zero_naive(foo) == bitSetSize );
}




TEST_CASE( " Amount of 1s in a binary string is counted correctly for a single use case", "[popcnt_naive]"){
    NaivePopcnt<64> naivePopcnt;
    int n = 3526; //0110111000110 - 7 bits set
    REQUIRE(naivePopcnt.naivePopcnt(n) == 7);
}




TEST_CASE(" Get Excitation Calculates Matrix values and phase correctly ", "[ExcitationCalculator]")
{
    //Do before each section - setup
    const int MONUM = 64;
    ExcitationCalculator<MONUM> ge;
    ArrayAllocator a;
    int *** array = a.allocate(3,2,2);
    long double phase = 0;
    unsigned long long degree;


    SECTION( " Single Excitation Calculates positive phase and correct matrix values ") {
        std::bitset<MONUM> det1up(32767);
        std::bitset<MONUM> det1down(16383);
        std::bitset<MONUM> det2up(549755845631);
        std::bitset<MONUM> det2down(4294983551);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase);
        REQUIRE(phase == 1.0);
        REQUIRE(array[0][0][0] == 1);
        REQUIRE(array[0][0][1] == 1);
        REQUIRE(array[0][1][0] == 1);
        REQUIRE(array[0][1][1] == 1);
        REQUIRE(array[1][0][0] == 11);
        REQUIRE(array[1][0][1] == 8);
        REQUIRE(array[1][1][0] == 40);
        REQUIRE(array[1][1][1] == 33);
        REQUIRE(array[2][0][0] == 0);
        REQUIRE(array[2][0][1] == 0);
        REQUIRE(array[2][1][0] == 0);
        REQUIRE(array[2][1][1] == 0);


    }

    SECTION( " Single Excitation Calculates negative phase and correct matrix values ") {
        std::bitset<MONUM> det1up(32767);
        std::bitset<MONUM> det1down(16383);
        std::bitset<MONUM> det2up(32767);
        std::bitset<MONUM> det2down(4402341493743);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase);
        REQUIRE(phase == -1.0);
        REQUIRE(array[0][0][0] == 0);
        REQUIRE(array[0][0][1] == 2);
        REQUIRE(array[0][1][0] == 0);
        REQUIRE(array[0][1][1] == 2);
        REQUIRE(array[1][0][0] == 0);
        REQUIRE(array[1][0][1] == 5);
        REQUIRE(array[1][1][0] == 0);
        REQUIRE(array[1][1][1] == 33);
        REQUIRE(array[2][0][0] == 0);
        REQUIRE(array[2][0][1] == 11);
        REQUIRE(array[2][1][0] == 0);
        REQUIRE(array[2][1][1] == 43);
    }

    SECTION( " Double Excitation Caculates negative phase and correct matrix values "){
        std::bitset<64> det1up (17179899903);
        std::bitset<64> det1down ( 17179883519);
        std::bitset<64> det2up (85932918783           );
        std::bitset<64> det2down (17179883519);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == -1.0);
        REQUIRE(array[0][0][0] == 2);
        REQUIRE(array[0][0][1] == 0);
        REQUIRE(array[0][1][0] == 2);
        REQUIRE(array[0][1][1] == 0);
        REQUIRE(array[1][0][0] == 13);
        REQUIRE(array[1][0][1] == 0);
        REQUIRE(array[1][1][0] == 26);
        REQUIRE(array[1][1][1] == 0);
        REQUIRE(array[2][0][0] == 14);
        REQUIRE(array[2][0][1] == 0);
        REQUIRE(array[2][1][0] == 37);
        REQUIRE(array[2][1][1] == 0);
    }

    SECTION( " Double Excitation Calculates negative phase and correct matrix values 2 ") {
        array[1][0][0] = 10;
        array[2][0][0] = 24;
        array[1][1][0] = 23;
        array[2][1][0] = 26;
        array[1][0][1] = 25;
        array[2][0][1] = 25;
        array[1][1][1] = 12;
        array[2][1][1] = 12;
        std::bitset<MONUM> det1up (32767);
        std::bitset<MONUM> det1down (25179135);
        std::bitset<MONUM> det2up (67133439);
        std::bitset<MONUM> det2down (16791551);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == -1.0);
        REQUIRE(array[0][0][0] == 1);
        REQUIRE(array[0][0][1] == 1);
        REQUIRE(array[0][1][0] == 1);
        REQUIRE(array[0][1][1] == 1);
        REQUIRE(array[1][0][0] == 14);
        REQUIRE(array[1][0][1] == 24);
        REQUIRE(array[1][1][0] == 27);
        REQUIRE(array[1][1][1] == 11);
        REQUIRE(array[2][0][0] == 24);
        REQUIRE(array[2][0][1] == 25);
        REQUIRE(array[2][1][0] == 26);
        REQUIRE(array[2][1][1] == 12);
    }

    SECTION( " Double Excitation Calculates positive phase and correct matrix values  ") {
        std::bitset<MONUM> det1up (32767);
        std::bitset<MONUM> det1down (16383);
        std::bitset<MONUM> det2up (16793599);
        std::bitset<MONUM> det2down (1099511644031);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == 1.0);
        REQUIRE(degree == 2.0);
        REQUIRE(array[0][0][0] == 1);
        REQUIRE(array[0][0][1] == 1);
        REQUIRE(array[0][1][0] == 1);
        REQUIRE(array[0][1][1] == 1);
        REQUIRE(array[1][0][0] == 15);
        REQUIRE(array[1][0][1] == 8);
        REQUIRE(array[1][1][0] == 25);
        REQUIRE(array[1][1][1] == 41);
        REQUIRE(array[2][0][0] == 0);
        REQUIRE(array[2][0][1] == 0);
        REQUIRE(array[2][1][0] == 0);
        REQUIRE(array[2][1][1] == 0);
    }

    SECTION( " Double Excitation Calculates positive phase and correct matrix values  2") {
        array[1][0][0] = 10;
        array[2][0][0] = 24;
        array[1][1][0] = 33;
        array[2][1][0] = 26;
        array[1][0][1] = 10;
        array[2][0][1] = 36;
        array[1][1][1] = 13;
        array[2][1][1] = 33;
        std::bitset<64> det1up (32767);
        std::bitset<64> det1down (42949684223);
        std::bitset<64> det2up (8420351);
        std::bitset<64> det2down (8589949951);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == 1.0);
        REQUIRE(degree == 2);
        REQUIRE(array[0][0][0] == 1);
        REQUIRE(array[0][0][1] == 1);
        REQUIRE(array[0][1][0] == 1);
        REQUIRE(array[0][1][1] == 1);
        REQUIRE(array[1][0][0] == 11);
        REQUIRE(array[1][0][1] == 36);
        REQUIRE(array[1][1][0] == 24);
        REQUIRE(array[1][1][1] == 13);
        REQUIRE(array[2][0][0] == 24);
        REQUIRE(array[2][0][1] == 36);
        REQUIRE(array[2][1][0] == 26);
        REQUIRE(array[2][1][1] == 33);
    }

    SECTION (" more than 2 excitations zeros matrix and phase"){
        std::bitset<MONUM> det1up (32767);
        std::bitset<MONUM> det1down (16383);
        std::bitset<MONUM> det2up (8594160127);
        std::bitset<MONUM> det2down (17184077311);
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == -1.0);
        REQUIRE(degree == 3);
        REQUIRE(array[0][0][0] == 0);
        REQUIRE(array[0][0][1] == 0);
        REQUIRE(array[0][1][0] == 0);
        REQUIRE(array[0][1][1] == 0);
        REQUIRE(array[1][0][0] == 0);
        REQUIRE(array[1][0][1] == 0);
        REQUIRE(array[1][1][0] == 0);
        REQUIRE(array[1][1][1] == 0);
        REQUIRE(array[2][0][0] == 0);
        REQUIRE(array[2][0][1] == 0);
        REQUIRE(array[2][1][0] == 0);
        REQUIRE(array[2][1][1] == 0);
    }

    SECTION (" Verify phase with single excitation "){
        std::bitset<64> det1up (32767);
        std::bitset<64> det1down (42949684223);
        std::bitset<64> det2up (32767);
        std::bitset<64> det2down (8403967);
        array[1][0][0] = 13;
        array[2][0][0] = 24;
        array[1][1][0] = 26;
        array[2][1][0] = 26;
        array[1][0][1] = 34;
        array[2][0][1] = 36;
        array[1][1][1] = 11;
        array[2][1][1] = 26;
        long double phase = 0;
        unsigned long long degree = 0;
        ge.getExcitation(det1up, det1down, det2up, det2down, array, degree, phase );
        REQUIRE(phase == -1.0);
        REQUIRE(degree == 2);
    }


    //Do after each section - tear down
    delete(array);
}

TEST_CASE("DECREMENTOR ") {

    BitStringDecrementor<64> d;

    for(int i = 1; i < 214748; i++){
        std::bitset<64> bitbuffer(i);

        REQUIRE(d.decrement(bitbuffer).to_ullong()==(i-1));
    }

    std::bitset<64> bitbuffer(2147483647);
    REQUIRE(d.decrement(bitbuffer).to_ullong()==(2147483646));
}


TEST_CASE( " n_excitations.excitations - 10000 64bit copper determinants",                     "[n_excitations]") {
    const int bitSetSize = 64;
    DetReader<bitSetSize> detReader;
    pair<vector<bitset<bitSetSize>>, vector<bitset<bitSetSize>>> dets = detReader.readFile("testData/cu_dat.bin");


    NExcitations<bitSetSize> n;
    int expectedResults[10000];
    int exc[3][2][2];
    string getcontent;
    ifstream openfile("testData/expected_9_excitations_cu_64_10000.txt");
    //expectedResults[0] = 0;
    if (openfile.is_open()) {
        long int amount = 0;
        getline(openfile, getcontent);
        while (!openfile.eof()) {
            getline(openfile, getcontent);

            vector<string> strs;
            boost::split(strs, getcontent, boost::is_any_of("\t "), boost::token_compress_on);

            for (size_t i = 1; i < strs.size(); i++) {

                istringstream is(strs[i]);
                unsigned long long int ii = 0;
                is >> ii;
                expectedResults[amount] = ii;
                amount++;
            }
        }
    }
    else{
        cout << "File not found. " << '\n';
        REQUIRE(false == true);
    }
    int count = 0;
    int ndet = 10000;

    for (int i = 0; i < dets.first.size(); i++) {

        REQUIRE(n.getExcitations(dets.first.at(i), dets.second.at(i), dets.first.at((ndet - 1) - i),
                                 dets.second.at((ndet - 1) - i)).to_ullong() ==
                expectedResults[i]);
    }
}



TEST_CASE("End to End", " DensityMatrtix is calculated correctly with 10,000 64bit cu determinants")
{

    const int bitsetSize = 64;
    DetReader<bitsetSize> dr;
    DensityMatrix<bitsetSize> d;
    vector<vector<double> > densityMatrix = d.computeDensityMatrix(0,10000, dr.readCoef("testData/cu.coef"),dr.readFile("testData/cu_dat.bin") );
    RelDif r;
    long double TOLERANCE = 0.0000000001;

    string getcontent;
    ifstream openfile("testData/expectedDensityMatrix");
    if (openfile.is_open()) {
        long int amount = 0;
        getline(openfile, getcontent);
        while (!openfile.eof()) {
            getline(openfile, getcontent);

            vector<string> strs;
            boost::split(strs, getcontent, boost::is_any_of("\t "), boost::token_compress_on);

            long double expectedValue;
            int densitryMatrixDimension1;
            int densitryMatrixDimension2;
            stringstream stream1(strs[2]);
            stream1 >> densitryMatrixDimension1;
            stringstream stream2(strs[3]);
            stream2 >> densitryMatrixDimension2;
            stringstream stream3(strs[1]);
            stream3 >> expectedValue;
            REQUIRE(r.dif(densityMatrix[densitryMatrixDimension1][densitryMatrixDimension2],     expectedValue)           <=TOLERANCE);

        }
    }


}

