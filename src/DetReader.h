//
// Created by chris on 27/12/2017.
//
using namespace std;
#include <bitset>
#include <vector>
#include <fstream>
#include <boost/algorithm/string/split.hpp>
#include <sstream>
#include <boost/algorithm/string/classification.hpp>

#ifndef UNTITLED1_DETREADER_H
#define UNTITLED1_DETREADER_H


template <int n> class DetReader{

public: pair <vector<bitset<n>>, vector<bitset<n>>> readFile(string fileName){
        pair <vector<bitset<n>>,vector<bitset<n>>> dets;

        int size = 0;
        bool up = true;
        string getcontent;
        ifstream openfile(fileName);
        if (openfile.is_open()) {
            long int amount = 0;

            while (!openfile.eof()) {
                getline(openfile, getcontent);

                    bitset<n> bs(getcontent);
                   // cout<< <<endl;


                if(getcontent.length()>size){
                    size = getcontent.length();
                }
                    if (up) {
                        dets.first.push_back(bs);
                        up = false;
                    } else {
                        dets.second.push_back(bs);
                        up = true;
                    }
                    amount = amount + 1;
            }
        }

       // cout <<"***************** " <<size <<endl;

        openfile.close();
        return dets;
    }

public: vector<long double> readCoef(string fileName){
        vector<long double> coef;

        bool up = true;
        string getcontent;
        ifstream openfile(fileName);
        if (openfile.is_open()) {
            long int amount = 0;

            while (!openfile.eof()) {
                getline(openfile, getcontent);

                vector<string> strs;
                boost::split(strs, getcontent, boost::is_any_of("\t "), boost::token_compress_on);
                for (size_t i = 1; i < strs.size(); i++) {
                    istringstream is(strs[i]);
                    long double ii = 0;
                    is >> ii;
                    coef.push_back(ii);
                    amount = amount + 1;
                }


            }
        }



        openfile.close();
        return coef;
    }


};
#endif //UNTITLED1_DETREADER_H
