
#include <sstream>
#include "../hayai/hayai.hpp"
#include "CountLeadingZeros.h"
#include "NaivePopcnt.h"
#include "DensityMatrtix.h"
#include "DetReader.h"
#include <boost/bind.hpp>
#include "libpopcnt.h"

#include <thread>


#include "boost/dynamic_bitset/dynamic_bitset.hpp"
#include "args.hxx"
int bits = 64;
int threadNum = 1;
std::vector <vector<vector<double> >>matrices;

int NDET;
std::string df, cf;

int main(int argc,char *argv[]){


    args::ArgumentParser parser(


            " __ _       _             ___                _                         \n"
                    "/ _\\ | __ _| |_ ___ _ __ / __\\___  _ __   __| | ___  _ __    _     _   \n"
                    "\\ \\| |/ _` | __/ _ \\ '__/ /  / _ \\| '_ \\ / _` |/ _ \\| '_ \\ _| |_ _| |_ \n"
                    "_\\ \\ | (_| | ||  __/ | / /__| (_) | | | | (_| | (_) | | | |_   _|_   _|\n"
                    "\\__/_|\\__,_|\\__\\___|_| \\____/\\___/|_| |_|\\__,_|\\___/|_| |_| |_|   |_|"

            "\n \n Generatres the density matrix of size [ndet][ndet] generated from a list of binary determinants and their coefficients.", "");

    args::Group required(parser, "Required arguments:", args::Group::Validators::All);
    args::ValueFlag<int> bitSetSize(required, "bitSetSize", "Number of determinants pairs.", {'b'});
    args::ValueFlag<int> ndet(required, "ndet", "Number of determinants pairs.", {'n'});
    args::ValueFlag<std::string>detFile(required, "detFile", "The file location of determinant pairs (binary format)", {"df"});
    args::ValueFlag<std::string> coefFile(required, "coefFile", "The file location of coefficients(floating point)", {"cf"});
    args::Group optional(parser, "Optional arguments:", args::Group::Validators::DontCare);
    args::ValueFlag<int> threads(optional, "threads", "Number of worker threads (1-32). Default 1.", {'t'});
    args::Flag benchmark(optional, "benchmark", "Turn on benchmarking - no determinant matrix output.", {"benchmark"});

    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

//    ndet
//            detFile
//    coefFile
//            threads
//    benchmark
    if (bitSetSize) { bits = args::get(bitSetSize)  ;}
    if (ndet) { NDET = args::get(ndet)  ;}
    if (detFile) {df = args::get(detFile);}
    if (coefFile) {cf = args::get(coefFile);}
    if (threads){ threadNum = args::get(threads);}

    ///threadNum, bits

    hayai::ConsoleOutputter consoleOutputter;
    hayai::Benchmarker::AddOutputter(consoleOutputter);
    hayai::Benchmarker::RunAllTests();


    return 0;
}

void calculateDensityMatrixSection64(int start, int end, std::vector<long double> coef,
                                   std::pair<std::vector<std::bitset<64>>, std::vector<std::bitset<64>>> det) {

    DensityMatrix<64> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}

void calculateDensityMatrixSection128(int start, int end, std::vector<long double> coef,
                                     std::pair<std::vector<std::bitset<128>>, std::vector<std::bitset<128>>> det) {

    DensityMatrix<128> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}

void calculateDensityMatrixSection256(int start, int end, std::vector<long double> coef,
                                      std::pair<std::vector<std::bitset<256>>, std::vector<std::bitset<256>>> det) {

    DensityMatrix<256> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}


void calculateDensityMatrixSection512(int start, int end, std::vector<long double> coef,
                                      std::pair<std::vector<std::bitset<512>>, std::vector<std::bitset<512>>> det) {

    DensityMatrix<512> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}


void calculateDensityMatrixSection1024(int start, int end, std::vector<long double> coef,
                                      std::pair<std::vector<std::bitset<1024>>, std::vector<std::bitset<1024>>> det) {

    DensityMatrix<1024> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}

void calculateDensityMatrixSection2048(int start, int end, std::vector<long double> coef,
                                       std::pair<std::vector<std::bitset<2048>>, std::vector<std::bitset<2048>>> det) {

    DensityMatrix<2048> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}


void calculateDensityMatrixSection4096(int start, int end, std::vector<long double> coef,
                                       std::pair<std::vector<std::bitset<4096>>, std::vector<std::bitset<4096>>> det) {

    DensityMatrix<4096> d;
    matrices.push_back(d.computeDensityMatrix(start,end,coef,det));

}



/**
 * Spawns n threads
 */

void spawnThreads(string detFile, string coefFile, int threadNum, int nDet, int bitStringSize)
{

    std::thread* threads = new std::thread [threadNum];
    unsigned long long detsSquared =  pow(nDet,2);

    if(bitStringSize==64) {

        std::pair<std::vector<std::bitset<64>>, std::vector<std::bitset<64>>> det;
        DetReader<64> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection64, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection64, detCounter, nDet, coef, det);

    }else if (bitStringSize ==128){

        std::pair<std::vector<std::bitset<128>>, std::vector<std::bitset<128>>> det;
        DetReader<128> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection128, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection128, detCounter, nDet, coef, det);
    }

    else if (bitStringSize ==256){

        std::pair<std::vector<std::bitset<256>>, std::vector<std::bitset<256>>> det;
        DetReader<256> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection256, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection256, detCounter, nDet, coef, det);
    }


    else if (bitStringSize ==512){

        std::pair<std::vector<std::bitset<512>>, std::vector<std::bitset<512>>> det;
        DetReader<512> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection512, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection512, detCounter, nDet, coef, det);
    }


    else if (bitStringSize ==1024){

        std::pair<std::vector<std::bitset<1024>>, std::vector<std::bitset<1024>>> det;
        DetReader<1024> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection1024, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection1024, detCounter, nDet, coef, det);
    }



    else if (bitStringSize ==2048){

        std::pair<std::vector<std::bitset<2048>>, std::vector<std::bitset<2048>>> det;
        DetReader<2048> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection2048, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection2048, detCounter, nDet, coef, det);
    }


    else if (bitStringSize ==4096){

        std::pair<std::vector<std::bitset<4096>>, std::vector<std::bitset<4096>>> det;
        DetReader<4096> dr;
        det = dr.readFile(detFile);
        std::vector<long double> coef;
        coef = dr.readCoef(coefFile);

        int detCounter = 0;
        for (int i = 0; i < threadNum - 1; i++) {
            double start = sqrt((detsSquared / threadNum) * i);
            double end = sqrt((detsSquared / threadNum) * (i + 1));

            int s = start;
            int e = end;

            threads[i] = std::thread(calculateDensityMatrixSection4096, s, e, coef, det);
            detCounter += end - start;
        }
        threads[threadNum - 1] = std::thread(calculateDensityMatrixSection4096, detCounter, nDet, coef, det);
    }

    threads->join();
}

BENCHMARK(density_matrix_10k, copper_64, 1, 1)
{
    spawnThreads(df, cf,threadNum,NDET, bits);
}
//
//BENCHMARK(density_matrix_10k_T2, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,2,10000);
//}
//
//BENCHMARK(density_matrix_10k_T4, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,4,10000);
//}
//
//BENCHMARK(density_matrix_10k_T8, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,8,10000);
//}
//
//BENCHMARK(density_matrix_10k_T16, copper_64,1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,16,10000);
//}
//
//BENCHMARK(density_matrix_10k_T32, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,32,10000);
//}



//

//
//
//
//BENCHMARK(density_matrix_100000_T1, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,1,100000);
//}
//

//
//BENCHMARK(density_matrix_10k_T1, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat.bin", "testData/cu.coef" ,1,10000);
//}

//BENCHMARK(popcount, copper_64, 200, 50)
//{
//    const int monum = 64;
//    bitset<monum> n(3012312039);
//    n.count();
//    //cout << n.count() << endl;
//
//
//
//}
//
//BENCHMARK(popcount64, copper_64, 200, 50)
//{
//    const int monum = 128;
//    uint64_t n [1] = {3012312039};
//    //
//  //  popcnt64(n);
//    popcnt64_unrolled(n, 1);
//  //  popcnt(&n, 8);
//
//
//}
//
//BENCHMARK(popcount1024, copper_64, 200, 50)
//{
//    const int monum = 1024;
//    bitset<monum> n(3012312039);
//    n.count();
//    //cout << n.count() << endl;
//
//
//
//}
//
//BENCHMARK(popcount641024, copper_64, 200, 50)
//{
//    const int monum = 128;
//    uint64_t n [16] = {3012312039};
//    //
//    //  popcnt64(n);
//    popcnt64_unrolled(n, 16);
//    //  popcnt(&n, 8);
//
//
//}
//


//BENCHMARK(density_matrix_100k_T1, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,1,100000);
//}
//
//BENCHMARK(density_matrix_100k_T2, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,2,100000);
//}
//
//BENCHMARK(density_matrix_100k_T4, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,4,100000);
//}
//
//BENCHMARK(density_matrix_100k_T8, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,8,100000);
//}
//
//BENCHMARK(density_matrix_100k_T16, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,16,100000);
//}

//BENCHMARK(density_matrix_100k_T32, copper_64, 1, 1)
//{
//    spawnThreads("testData/cu_dat_100k", "testData/cu_coef_100k" ,32,100000);
//}
//
//BENCHMARK(density_matrix_10000_T2, copper_64, 1, 1)
//{
//    spawnThreads(2,100000);
//}
//
//BENCHMARK(density_matrix_10000_T3, copper_64, 1, 1)
//{
//    spawnThreads(3,100000);
//}
//
//BENCHMARK(density_matrix_10000_T4, copper_64, 1, 1)
//{
//    spawnThreads(4,100000);
//}
//
//BENCHMARK(density_matrix_10000_T5, copper_64, 1, 1)
//{
//    spawnThreads(5,100000);
//}
//
//BENCHMARK(density_matrix_10000_T6, copper_64, 1, 1)
//{
//    spawnThreads(6,100000);
//}
//
//BENCHMARK(density_matrix_10000_T7, copper_64, 1, 1)
//{
//    spawnThreads(7,100000);
//}
//
//BENCHMARK(density_matrix_10000_T8, copper_64, 1, 1)
//{
//    spawnThreads(8,100000);
//}
//
//BENCHMARK(density_matrix_10000_T9, copper_64, 1, 1)
//{
//    spawnThreads(9,100000);
//}
//
//BENCHMARK(density_matrix_10000_T10, copper_64, 1, 1)
//{
//    spawnThreads(10,100000);
//}
//
//BENCHMARK(density_matrix_10000_T11, copper_64, 1, 1)
//{
//    spawnThreads(11,100000);
//}
//
//
//BENCHMARK(density_matrix_10000_T12, copper_64, 1, 1)
//{
//    spawnThreads(12,100000);
//}
//
//BENCHMARK(density_matrix_10000_T13, copper_64, 1, 1)
//{
//    spawnThreads(13,100000);
//}
//
//BENCHMARK(density_matrix_10000_T14, copper_64, 1, 1)
//{
//    spawnThreads(14,100000);
//}
//
//BENCHMARK(density_matrix_10000_T15, copper_64, 1, 1)
//{
//    spawnThreads(15,100000);
//}
//
//BENCHMARK(density_matrix_10000_T16, copper_64, 1, 1)
//{
//    spawnThreads(16,100000);
//}


//BENCHMARK(density_matrix_10000_T1, copper_64, 1, 1)
//
//{
    //spawnThreads(1,10000);
  //  spawnThreads(2,10000);


//    DensityMatrix<64> d;
//
//    //boost::thread_group tgroup;
//
//    boost::thread t3(&DensityMatrix::computeDensityMatrix, d);
//    t3.join();
    //tgroup.create_thread(boost::bind( SLATERCONDONPP_DENSITY_MATRIX_H::DensityMatrix::computeDensityMatrix,
                                     // d,0,10000,dr.readFile("testData/cu_dat.bin"), 10000, dr.readCoef("testData/cu.coef") ));
    //d.computeDensityMatrix(0,10000,dr.readFile("testData/cu_dat.bin"), 10000, dr.readCoef("testData/cu.coef"));
    //tgroup.join_all();


//}



//
//BENCHMARK(density_matrix_64bit, copper_100, 1, 1)
//{
//    DetReader<64> dr;
//    DensityMatrix<64> d;
//    d.computeDensityMatrix(dr.readFile("testData/cu.dat"), 100, dr.readCoef("testData/cu.coef"));
//}
//
//BENCHMARK(density_matrix_64bit, copper_1000, 1, 1)
//{
//    DetReader<64> dr;
//    DensityMatrix<64> d;
//    d.computeDensityMatrix(dr.readFile("testData/cu.dat"), 1000, dr.readCoef("testData/cu.coef"));
//}
//
//BENCHMARK(density_matrix_64bit, copper_10000, 1, 1)
//{
//    DetReader<64> dr;
//    DensityMatrix<64> d;
//    d.computeDensityMatrix(dr.readFile("testData/cu.dat"), 10000, dr.readCoef("testData/cu.coef"));
//}

//
//BENCHMARK(kernaghansPopcnt64bit, popcnt , 10, 50) {
//    NaivePopcnt<64> p;
//    std::bitset<64> n = std::bitset<64>(1);
//    for(int i = 1; i < 64; i++){
//        n<<=1;
//        n[0]=1;
//        p.brianKernighanPopcnt(n);
//    }
//
//}
//BENCHMARK(dsadad,asdasdad,1,1){
//    eastl::bitset<64> bs(2);
////trailing
//    for(int i = 1; i < 64; i++){
//        //p.naivePopcnt(n);
//
////        cout << bs.to_uint64() << endl;
////       cout<< bs.DoFindFirst() <<endl;
//
//    }
//
//
//}

//BENCHMARK(dsadaad,asdasdad,1,1){
//    eastl::bitset<64> zero(0);
//   // cout << zero.DoFindLast() << endl;
//    cout << (zero.DoFindFirst()) << endl;
//    eastl::bitset<64> bs(1);
//
//
//
//    for(int i = 1; i < 64; i++){
//
//
//        cout << (64 - (bs.DoFindFirst()+1)) << endl;
//        bs<<=1;
//
//
//    }
//
//
//}
//
//
//BENCHMARK(CountLeadingZerosEastl, popcnt , 10, 500) {
//    eastl::bitset<64> zero = eastl::bitset<64>(0);
//    CountLeadingZeros<int, 64> p;
//    //cout << p.trailing_zero_eastl(zero) << endl;
//    // cout << "***" << endl;
//    eastl::bitset<64> n = eastl::bitset<64>(1);
//    for(int i = 1; i < 64; i++){
//        //p.naivePopcnt(n);
//
//        // p.leading_zero_naive(n);
//       // cout << p.trailing_zero_eastl(n) << endl;
//        p.trailing_zero_eastl(n);
//        n<<=1;
//        //cout <<  p.leading_zero_naive(n) << endl;
//        //cout << i << endl;
//    }
//}
//
//
//BENCHMARK(CountLeadingZeros, popcnt , 10, 500) {
//    std::bitset<64> zero = std::bitset<64>(0);
//    CountLeadingZeros<int, 64> p;
//    //cout << p.leading_zero_naive(zero) << endl;
//   // cout << "***" << endl;
//    std::bitset<64> n = std::bitset<64>(1);
//    for(int i = 1; i < 64; i++){
//        //p.naivePopcnt(n);
//
//       // p.leading_zero_naive(n);
//       // cout << p.leading_zero_naive(n) << endl;
//        n<<=1;
//        //cout <<  p.leading_zero_naive(n) << endl;
//        //cout << i << endl;
//    }
//}

//
//BENCHMARK(CountTrailing, popcnt , 10, 500) {
//    CountTrailingZeros<int, 64> p;
//    std::bitset<64> n = std::bitset<64>(1);
//    for(int i = 1; i < 64; i++){
//        //p.naivePopcnt(n);
//        n<<=1;
//        p.trailing_zero_naive(n);
//    }
//}

//
////NAIVE POPCNT
////
//BENCHMARK(NaivePopcnt64bit, popcnt , 10, 500) {
//    NaivePopcnt<64> p;
//    std::bitset<64> n = std::bitset<64>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//    }
//}
////
//BENCHMARK(NaivePopcnt128bit, popcnt , 10, 50) {
//    NaivePopcnt<128> p;
//    std::bitset<128> n = std::bitset<128>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//    }
//}
//
//
//BENCHMARK(NaivePopcnt256bit, popcnt , 10, 50) {
//    NaivePopcnt<256> p;
//    std::bitset<256> n = std::bitset<256>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//
//    }
//}
//
//BENCHMARK(NaivePopcnt512bit, popcnt , 10, 50) {
//    NaivePopcnt<512> p;
//    std::bitset<512> n = std::bitset<512>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//
//    }
//}
//
//BENCHMARK(NaivePopcnt1024bit, popcnt , 10, 50) {
//    NaivePopcnt<1024> p;
//    std::bitset<1024> n = std::bitset<1024>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//
//    }
//}
//
//BENCHMARK(NaivePopcnt2048bit, popcnt , 10, 50) {
//    NaivePopcnt<2048> p;
//    std::bitset<2048> n = std::bitset<2048>(1);
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//
//    }
//}
//
//BENCHMARK(NaivePopcnt4096bit, popcnt , 10, 50) {
//    NaivePopcnt<4096> p;
//    std::bitset<4096> n = std::bitset<4096>(1);
//    n = n.flip();
//    for(int i = 1; i < 64; i++){
//        p.naivePopcnt(n);
//
//    }
//}
//
////BOOST DYNAMIC BITSET
//BENCHMARK_P(dynamic_bitset, count, 10, 50,
//            ( int bitSetSize))
//{
//    boost::dynamic_bitset<> n(bitSetSize);
//
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//
//
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (64));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (128));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (256));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (512));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (1024));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (2048));
//BENCHMARK_P_INSTANCE(dynamic_bitset, count, (4096));
//
//
//
////STD BITSET
//BENCHMARK(bitset64bit, count , 10, 50) {
//    std::bitset<64> n = std::bitset<64>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset128bit, count , 10, 50) {
//    std::bitset<128> n = std::bitset<128>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset256bit, count , 10, 50) {
//    std::bitset<256> n = std::bitset<256>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset512bit, count , 10, 50) {
//    std::bitset<512> n = std::bitset<512>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset1024bit, count , 10, 50) {
//    std::bitset<1024> n = std::bitset<1024>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset2048bit, count , 10, 50) {
//    std::bitset<2048> n = std::bitset<2048>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}
//BENCHMARK(bitset4096bit, count , 10, 50) {
//    std::bitset<4096> n = std::bitset<4096>(1);;
//    for(int i = 1; i < 64; i++){
//        n.count();
//    }
//}