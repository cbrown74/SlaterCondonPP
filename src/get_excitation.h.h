//
// Created by chris on 31/12/2017.
//
using namespace std;
#ifndef UNTITLED1_GET_EXCITATION_H_H
#define UNTITLED1_GET_EXCITATION_H_H

#include <bitset>
#include "NExcitations.h"
#include "DoubleExcitationCalculator.h"
#include "SingleExcitationCalculator.h"

// For the identification of the substitutions, only four
// cases are possible (figure 2) depending of the degree
// of excitation :

// no substitution, -the subroutine exits with a degree of excitation of zero
// one substitution,
// two substitutions
// more than two substitutions - returns a degrees of excitation equal to -1

//For the two remaining
// cases, a particular subroutine is written for each case.

//The cases were ordered from the most probable to the
//least probable to optimize the branch prediction.

//. In output, the indices of the spin-orbitals are given in
//the array exc as follows:
//• The last index of exc is the spin (1 for α and 2
//for β)
//• The second index of exc is 1 for holes and 2 for
//particles
//• The element at index 0 of the first dimension of
//        exc gives the total number of holes or particles
//        of spin α or β
//• The first index of exc refers to the orbital index
//of the hole or particle


template <int moNum> class ExcitationCalculator{


    NExcitations<moNum> nex;
    DoubleExcitationCalculator<moNum> doubleExctations;
    //The exc and degree passed in need to be changed inside the class which is calling the function
    //They are passed by reference
    public: void getExcitation(bitset<moNum> det1up, bitset<moNum> det1down,
                              bitset<moNum>det2up, bitset<moNum>det2down, int ***exc, unsigned long long &degree, long double &phase){



            degree = nex.getExcitations(det1up, det1down, det2up, det2down).to_ullong();

            if(degree>2){
                degree = 3;
                phase = -1;
            }
            else if (degree==2){
                DoubleExcitationCalculator<moNum> g;
                g.getDoubleExcitation(det1up, det1down, det2up, det2down, exc, phase);
            }
            else if (degree ==1){

                SingleExcitationCalculator<moNum> g;
                g.getSingleExcitation(det1up, det1down, det2up, det2down, exc, phase);
            }

            else if (degree ==0){
                phase = 0.0;
                degree = 0;
            }
        }
};

#endif //UNTITLED1_GET_EXCITATION_H_H
