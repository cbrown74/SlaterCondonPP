//
// Created by Brown, Chris on 31/03/2018.
//

#ifndef SLATERCONDONPP_THREEDIMENSIONARRAYALLOCATOR_H
#define SLATERCONDONPP_THREEDIMENSIONARRAYALLOCATOR_H


class ArrayAllocator{
public: int*** allocate(int dimX, int dimY, int dimZ)
    {

        int ***array;    // 3D array definition;
        // begin memory allocation
        array = new int **[dimX];
        for (int x = 0; x < dimX; ++x) {
            array[x] = new int *[dimY];
            for (int y = 0; y < dimY; ++y) {
                array[x][y] = new int[dimZ];
                for (int z = 0; z < dimZ; ++z) { // initialize the values to whatever you want the default to be
                    array[x][y][z] = 0;
                }
            }
        }

        return array;
    }
};
#endif //SLATERCONDONPP_THREEDIMENSIONARRAYALLOCATOR_H
