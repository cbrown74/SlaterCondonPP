//
// Created by chris on 14/01/2018.
//

#ifndef UNTITLED1_COUNTTRAILINGZEROS_H
#define UNTITLED1_COUNTTRAILINGZEROS_H



#include <bitset>

template <typename ReturnType, int moNum> class CountTrailingZeros {







public: ReturnType trailing_zero_naive(std::bitset< moNum > x)
    {
//        std::bitset<moNum> n(1);
//        ReturnType t = 0;
//        while ((x & n) == 0)
//        {
//            x = x >> 1;
//            t++;
//        }
//        return t;

        ReturnType t = 0;
        for (int i = 0; i < moNum; i ++) {
            if( x[i]==1){
                break;
            }
            t++;
        }
        return t;
    }

};


#endif //UNTITLED1_COUNTTRAILINGZEROS_H
