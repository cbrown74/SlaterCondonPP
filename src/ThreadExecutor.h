////
//// Created by Brown, Chris on 15/04/2018.
////
//
//#ifndef SLATERCONDONPP_THREADEXECUTOR_H
//#define SLATERCONDONPP_THREADEXECUTOR_H
//
//using namespace std;
//const int monum=64;
//std::vector <std::vector<std::vector<double> >> matrices;
//template <int moNum> class ThreadExecutor{
//
//
//
//
//
//public: void calculateDensityMatrixSection(int start, int end, std::vector<long double> coef,
//                                   std::pair<std::vector<std::bitset<monum>>, std::vector<std::bitset<monum>>> det) {
//DensityMatrix<monum> d;
//matrices.push_back(d.computeDensityMatrix(start,end,coef,det));
//}
//
///**
// * Spawns n threads
// */
//
//void spawnThreads(string detFile, string coefFile, int threadNum, int nDet)
//{
//
//    std::thread* threads = new std::thread [threadNum];
//
//    std::pair<std::vector<std::bitset<monum>>, std::vector<std::bitset<monum>>> det;
//    std::vector<long double> coef;
//    DetReader<monum> dr;
//
//
//    det = dr.readFile(detFile);
//    coef = dr.readCoef(coefFile);
//
//
//    unsigned long long detsSquared =  pow(nDet,2);
//
//    int detCounter = 0;
//    for(int i = 0; i < threadNum-1; i++){
//        double start = sqrt((detsSquared/threadNum) * i );
//        double end = sqrt((detsSquared/threadNum) * (i+1) );
//
//        int s = start;
//        int e = end;
//
//        threads[i] = std::thread(calculateDensityMatrixSection,start ,end ,coef,det);
//        detCounter += end-start;
//    }
//    threads[threadNum-1] = std::thread(calculateDensityMatrixSection, detCounter , nDet ,coef,det);
//
//    threads->join();
//
//}
//
//};
//
//#endif //SLATERCONDONPP_THREADEXECUTOR_H
