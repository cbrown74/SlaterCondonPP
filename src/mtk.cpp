//
// Created by chris on 05/12/2017.



#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file



#include "CountLeadingZeros.h"
#include "DetReader.h"
#include "NaivePopcnt.h"
#include "NExcitations.h"
#include "get_excitation.h.h"
#include "HoleParticleCalculator.h"
#include "CountTrailingZeros.h"
#include "SingleExcitationCalculator.h"
#include "DensityMatrtix.h"
#include "RelDif.h"
#include "BitStringDecrementor.h"

int main(){


    const int bitsetSize = 64;
    DetReader<bitsetSize> dr;
    DensityMatrix<bitsetSize> d;
    vector<vector<double> > densityMatrix = d.computeDensityMatrix(0,10000, dr.readCoef("testData/cu.coef"),dr.readFile("testData/cu_dat.bin") );
    RelDif r;
    long double TOLERANCE = 0.0000000001;

    string getcontent;

    for(int i = 0; i < 10000; i ++){
        for (int j = 0; j < 10000; j++){
            cout << densityMatrix[i][j] << endl;
        }
    }

}


