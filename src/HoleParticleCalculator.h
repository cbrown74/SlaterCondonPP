//
// Created by chris on 03/01/2018.
//

#ifndef UNTITLED1_GET_HOLES_AND_PARTICLES_H
#define UNTITLED1_GET_HOLES_AND_PARTICLES_H


#include <utility>
#include <bitset>
#include <iostream>
#include "CountTrailingZeros.h"

template <int moNum> class HoleParticleCalculator{
    CountTrailingZeros<int,moNum> c;
    int tz;

public: void processExc(std::bitset<moNum> det1,std::bitset<moNum> det2, int ***exc){
            pair<bitset<moNum>, bitset<moNum>> p = getHolesAndParticles(det1,det2);
            processHolesAndParticles(p, exc);
    }

public: void processHolesAndParticles(pair<bitset<moNum>, bitset<moNum>> p, int ***exc){
             bitset<moNum>zero(0);

            //particle
            if (p.first !=zero ) {
                tz = c.trailing_zero_naive(p.first);
                exc[0][1][0] = 1;
                exc[1][1][0] = tz;
            }

            //hole
            if (p.second !=zero ) {
                tz = c.trailing_zero_naive(p.second);
                exc[0][0][0] = 1;
                exc[1][0][0] = tz;
            }
    }
public: std::pair<std::bitset<moNum>,std::bitset<moNum>> getHolesAndParticles(std::bitset<moNum> det1,std::bitset<moNum> det2){

        std::bitset<moNum> tmp = (det1 ^ det2);
        std::bitset<moNum> particle = tmp & det2;
        std::bitset<moNum> hole = tmp & det1;
        std::pair <std::bitset<moNum> , std::bitset<moNum>> p (particle, hole);
        return p;
    }
};

#endif //UNTITLED1_GET_HOLES_AND_PARTICLES_H
