//
// Created by chris on 03/01/2018.
//

#ifndef UNTITLED1_GET_DOUBLE_EXCITATION_H
#define UNTITLED1_GET_DOUBLE_EXCITATION_H

#include <bitset>
#include <iostream>
#include "CountTrailingZeros.h"
#include "BitStringDecrementor.h"

using namespace std;

template <int moNum> class DoubleExcitationCalculator{
public: void getDoubleExcitation(bitset<moNum> det1up, bitset<moNum> det1down,
                                 bitset<moNum> det2up, bitset<moNum> det2down, int ***exc, long double &phase) {


        std::bitset<moNum> l, ispin, i,j,k, hole, particle, tmp;
        int a,b,c,d,tz;
        std::bitset<moNum> zero(0);
        BitStringDecrementor<moNum> bitStringDecrementor;
        int nexc, idx_particle, idx_hole, nn, low, high, m;
        std::bitset<moNum> det1[] = {det1up, det1down};
        std::bitset<moNum> det2[] = {det2up, det2down};
        long double  phase_dble[] = {1, -1};

        exc[0][0][0] = 0;
        exc[0][1][0] = 0;
        exc[0][0][1] = 0;
        exc[0][1][1] = 0;

        CountTrailingZeros<int, moNum> cz;
        nexc = 0;
        long int nperm = 0;

        for (int iSpin = 0; iSpin < 2; iSpin++) {
            idx_particle = 0;
            idx_hole = 0;

            if (det1[iSpin] == det2[iSpin]) {
                continue;
            }

            tmp = det1[iSpin] ^ det2[iSpin];
            particle = tmp & det2[iSpin];
            hole = tmp & det1[iSpin];


            while(particle!=zero){
                nexc++;
                idx_particle ++;
                tz = cz.trailing_zero_naive(particle);
                exc[0][1][iSpin] = exc[0][1][iSpin]+1 ;
                exc[idx_particle][1][iSpin] = tz + 1;
                particle = particle & bitStringDecrementor.decrement(particle);
            }

            while(hole!=zero){
                nexc++;
                idx_hole ++;
                tz = cz.trailing_zero_naive(hole);
                exc[0][0][iSpin] = exc[0][0][iSpin]+1;
                exc[idx_hole][0][iSpin] = tz + 1;
                hole = hole & bitStringDecrementor.decrement(hole);
            }



            for(int excitation = 0; excitation < (exc[0][0][iSpin]); excitation++){

                low = std::min(exc[excitation+1][0][iSpin], exc[excitation+1][1][iSpin]);
                high = std::max(exc[excitation+1][0][iSpin], exc[excitation+1][1][iSpin]);
                nn = (low - 1) & (moNum-1);
                m = (high - 1) & (moNum-1);
                nperm = nperm +
                        std::bitset<moNum>(
                                det1[iSpin]&
                                        ( std::bitset<moNum> (~(1<<(nn+1))+1) & bitStringDecrementor.decrement(std::bitset<moNum>(1)<<m))
                                          ).count();

            }

            if (exc[0][0][iSpin] == 2){

                a = std::min(exc[1][0][iSpin], exc[1][1][iSpin]);
                b = std::max(exc[1][0][iSpin], exc[1][1][iSpin]);
                c = std::min(exc[2][0][iSpin], exc[2][1][iSpin]);
                d = std::max(exc[2][0][iSpin], exc[2][1][iSpin]);

                if(c>a && c<b && d>b){
                    nperm++;
                    break;
                }
            }

        }

        phase = phase_dble[nperm & 1];
    }

};
#endif //UNTITLED1_GET_DOUBLE_EXCITATION_H
