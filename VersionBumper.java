import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class VersionBumper {

    static int[] versionNumbers;
    //args [0]
    //0 = minorminor versionBump X.X.++1
    //1 = majorminor versionBump X.++1.X
    //2 = majormajor versionBump ++1.X.X

    public static void main(String[] args) throws FileNotFoundException {

        Scanner in;
        try {
            in = new Scanner(new FileReader("version.v"));
        } catch (FileNotFoundException e) {
            System.err.print("version.v not found");
            throw e;
        }

        String version = in.next();
        in.close();
        String[] tmp = version.split("\\.");
        versionNumbers = new int[3];

        versionNumbers[2] = Integer.parseInt(tmp[0]);
        versionNumbers[1] = Integer.parseInt(tmp[1]);
        versionNumbers[0] = Integer.parseInt(tmp[2]);

        bump(Integer.parseInt(args[0]));
        System.out.println(versionNumbers[2]+"."+versionNumbers[1]+"."+versionNumbers[0]);
    }

    public static void bump(int selection){
        switch (selection) {
            case 0:
                versionNumbers[0]++;
                break;
            case 1:
                versionNumbers[1]++;
                versionNumbers[0]=0;
                break;
            case 2:
                versionNumbers[2]++;
                versionNumbers[1]=0;
                versionNumbers[0]=0;
                break;
        }
    }


}
