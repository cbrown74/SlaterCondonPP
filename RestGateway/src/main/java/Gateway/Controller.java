package Gateway;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class Controller {


    String title =
            "  ________.__          __               _________                   .___                                     \n" +
                    " /   _____|  | _____ _/  |_  ___________\\_   ___ \\  ____   ____   __| _/____   ____      .__         .__     \n" +
                    " \\_____  \\|  | \\__  \\\\   ___/ __ \\_  __ /    \\  \\/ /  _ \\ /    \\ / __ |/  _ \\ /    \\   __|  |___   __|  |___ \n" +
                    " /        |  |__/ __ \\|  | \\  ___/|  | \\\\     \\___(  <_> |   |  / /_/ (  <_> |   |  \\ /__    __/  /__    __/ \n" +
                    "/_______  |____(____  |__|  \\___  |__|   \\______  /\\____/|___|  \\____ |\\____/|___|  /    |__|        |__|    \n" +
                    "        \\/          \\/          \\/              \\/            \\/     \\/           \\/                         ";

    private final Logger logger = LoggerFactory.getLogger(Controller.class);

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = System.getProperty("user.dir");

    @RequestMapping("/andy")
    public String index(){



        return title;

    }


    // 3.1.1 Single file upload
    @PostMapping("/api/upload")
    // If not @RestController, uncomment this
    //@ResponseBody
    public ArrayList<String[]>  uploadFile(
            @RequestParam("file") MultipartFile uploadfile) throws IOException, InterruptedException {

        long startTm = System.currentTimeMillis();
        logger.debug("Single file upload!");
        ArrayList<String> determinants;
//        if (uploadfile.isEmpty()) {
//            return new ResponseEntity("please select a file!", HttpStatus.OK);
//        }

        try {
            determinants = saveUploadedFiles(Arrays.asList(uploadfile));
        } catch (IOException e) {
            //return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

//        if(determinants==null){
//           // return new ResponseEntity<>(title+ "\n \n Syntax error, file should contain determinants seperated by whitespace only.",HttpStatus.BAD_REQUEST);
//        }


        //Process p = Runtime.getRuntime().exec("C:\\Users\\chris\\Desktop\\gitlab2\\SlaterCondonPP\\gateway.exe");
       // Process p = new ProcessBuilder("C:\\Users\\chris\\Desktop\\gitlab2\\SlaterCondonPP\\gateway.exe").start();


        // A Runtime object has methods for dealing with the OS
        Runtime r = Runtime.getRuntime();
        Process p;     // Process tracks one external native process
        BufferedReader is;  // reader for output of process
        String line = null;

        // Our argv[0] contains the program to run; remaining elements
        // of argv contain args for the target program. This is just
        // what is needed for the String[] form of exec.
//        p = r.exec("C:\\Users\\chris\\Desktop\\gitlab2\\SlaterCondonPP\\gateway.exe");
//
//        System.out.println("In Main after exec");
//
//        // getInputStream gives an Input stream connected to
//        // the process p's standard output. Just use it to make
//        // a BufferedReader to readLine() what the program writes out.
//        is = new BufferedReader(new InputStreamReader(p.getInputStream()));
//        Thread.sleep(2000);
//        while ((line = is.readLine()) != null)
//            System.out.println(line);
//
//        System.out.println("In Main after EOF");
//        System.out.flush();
//        try {
//            p.waitFor();  // wait for process to complete
//        } catch (InterruptedException e) {
//            System.err.println(e);  // "Can'tHappen"
//
//        }
//        System.err.println("Process done, exit status was " + p.exitValue());
        Thread.sleep(1000);
        BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\chris\\Desktop\\gitlab2\\SlaterCondonPP\\RestGateway\\src\\main\\java\\Gateway\\expectedDensityMatrix"));
        String rs;
        ArrayList<String[]> al = new ArrayList<>();
        while ((line = br.readLine()) != null)
        {
            //System.out.println("YA");
            if(line.split("\\s+").length==4){
                String[]temp = line.split("\\s+");
                String[] entry = {temp[1],temp[2],temp[3]};

                al.add(entry);
            }

        }

        return  al;
                //ResponseEntity(title +" \n \n Determinants found: "+determinants.size() +" \n Time taken: " + (System.currentTimeMillis() - startTm)+" milliseconds.", new HttpHeaders(), HttpStatus.OK);

    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    private   ArrayList<String> saveUploadedFiles(List<MultipartFile> files) throws IOException {

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue;
            }


            ArrayList<String> determinants = new ArrayList<>();

            byte[] bytes = file.getBytes();

            String content = new String(bytes);
            String temp[] = content.split("\\s+");
            String inputValidation = "^[0-9]*$";

            for(int i = 0; i < temp.length; i++){

                if(!temp[i].isEmpty()) {
                    if (temp[i].matches(inputValidation)) {
                        determinants.add(temp[i]);
                    } else {
                        return null;
                    }
                }

            }

//            for(int i = 0; i < determinants.size();i++){
//                logger.info(determinants.get(i));
//            }

            return determinants;
//            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
//            Files.write(path, bytes);

        }
        return null;

    }


}
