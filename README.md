<a id="top"></a>
![slatercondon++ logo](https://i.imgur.com/ZHyDjex.png)

[![pipeline](https://gitlab.com/cbrown74/SlaterCondonPP/badges/master/pipeline.svg)](https://gitlab.com/cbrown74/SlaterCondonPP/commits/master) 

A C++ implementation of Slater Condon rules.

### [Binaries](http://ec2-18-218-172-215.us-east-2.compute.amazonaws.com:8000/)

### Install
```
cmake .
```
-- Configuring done
-- Generating done
-- Build files have been written to: ../SlaterCondonPP

### Running the tests
```
#build
make RegressionTests
make PerformanceTests
#run 
./RegressionTests
./PerformanceTests
```

## Software

* [Hayai](https://github.com/nickbruun/hayai) - Performance test framework
* [catch2](https://github.com/catchorg/Catch2) - Test Framework
* [Boost](http://www.boost.org/) 

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
*  Charles Gillan - Initial project supervisor at Queen's Unversity Belfast
*  Based on research paper [An efficient implementation of Slater-Condon rules
   Anthony Scemama∗, Emmanuel Giner November 26, 2013](https://arxiv.org/pdf/1311.6244.pdf). Original [fortran implementation](https://github.com/scemama/slater_condon) here.


